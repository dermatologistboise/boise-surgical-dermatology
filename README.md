**Boise surgical dermatology**

Welcome to Idaho's Dermatologic Surgery & Laser Center. 
We are a Boise, Idaho-based dermatological medical practice specializing on skin cancer, dermatological surgery, and cosmetic procedures. 
When you visit our practice, we want you to feel as if you are with us. 
That is why our goal is to provide you with the best possible care we can.
Please Visit Our Website [Boise surgical dermatology](https://dermatologistboise.com/surgical-dermatology.php) for more information. 

---

## Our surgical dermatology in Boise

A specialized, highly successful treatment for the elimination of skin cancer is surgical dermatology in Boise. 
At the University of Wisconsin, the methodology was developed .
In the 1930s and is now being performed internationally. Boise Surgical Dermatology differs from most skin 
cancer procedures in that it allows for the elimination of cancerous tissue for rapid and complete microscopic examination, 
ensuring that both "roots" and cancer extensions can be removed.
Because of the methodological manner in which tissue is removed and tested, Surgical Dermatology in 
Boise has been recognized as the skin cancer treatment with the best reported cure rate.

